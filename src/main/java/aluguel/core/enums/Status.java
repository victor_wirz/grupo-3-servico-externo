package aluguel.core.enums;

public enum Status {
    ATIVO, 
    INATIVO, 
    AGUARDANDO_CONFIRMACAO
}
