package aluguel.core.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartaoDeCredito {
    private UUID id;
    private String nomeTitular;
    private String numero;

    @JsonFormat(pattern = "YYYY-MM-dd")
    private Date validade;
    
    private String cvv;
    private UUID ciclista;
}
