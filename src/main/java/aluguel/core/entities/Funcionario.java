package aluguel.core.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Funcionario {
    private String matricula;
    private String senha;
    private String email;
    private String nome;
    private Integer idade;
    private String funcao;
    private String cpf;
}
