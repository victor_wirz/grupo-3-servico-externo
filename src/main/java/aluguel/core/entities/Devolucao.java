package aluguel.core.entities;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Devolucao {
    private UUID bicicleta;

    @JsonFormat(pattern = "YYYY-MM-dd HH-mm-ss")
    private Date horaInicio;

    private UUID trancaFim;

    @JsonFormat(pattern = "YYYY-MM-dd HH-mm-ss")
    private Date horaFim;
    
    private UUID cobranca;
    private UUID ciclista;
}
