package aluguel.core.repositories;

import aluguel.core.entities.Funcionario;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class FuncionarioRepository {
    private Map<String, Funcionario> funcionarios;

    public FuncionarioRepository(){
        this.funcionarios = new ConcurrentHashMap<>();
        Funcionario f1 = new Funcionario("0ef4982c9569422693bf945fb4d8897b", "senha1", "funcionario1@email", "funcionario1", 24, "Funcao1", "xxx.xxx.xxx-xx");
        Funcionario f2 = new Funcionario("6a244ffccbe344edbd7a3fb91bd05369", "senha2", "funcionario2@email", "funcionario2", 25, "Funcao2", "xxx.xxx.xxx-xy");
        Funcionario f3 = new Funcionario("fcf9c76aa99f42e7ba0d590857b87594", "senha3", "funcionario3@email", "funcionario3", 26, "Funcao3", "xxx.xxx.xxx-yy");

        this.funcionarios.put(f1.getMatricula(), f1);
        this.funcionarios.put(f2.getMatricula(), f2);
        this.funcionarios.put(f3.getMatricula(), f3);
    }

    public void save(Funcionario f) {
        f.setMatricula(UUID.randomUUID().toString());

        this.funcionarios.put(f.getMatricula(), f);
    }

    public Collection<Funcionario> findAll() {
        return this.funcionarios.values();
    }

    public Optional<Funcionario> findById(String matricula) {
        return Optional.ofNullable(this.funcionarios.get(matricula));
    }

    public void update(String matricula, Funcionario f) {
        this.funcionarios.put(matricula, f);
    }

    public void delete(String matricula) {
        this.funcionarios.remove(matricula);
    }
}
