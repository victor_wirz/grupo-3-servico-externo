package aluguel.core.repositories;

import aluguel.core.entities.Aluguel;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AluguelRepository {
    private Map<UUID, Aluguel> alugueis;

    public AluguelRepository(){
        this.alugueis = new ConcurrentHashMap<>();
    }

    public Optional<Aluguel> getOngoingAluguelByCiclista(UUID ciclistaId) {
        return this.alugueis.values().stream()
            .filter(alug -> 
                alug.getCiclista().equals(ciclistaId) && 
                alug.getTrancaFim() == null
            ).findFirst();
    }

    public Optional<Aluguel> getOngoingAluguelByBicicleta(UUID bicicletaId) {
        return this.alugueis.values().stream()
            .filter(alug -> 
                alug.getBicicleta().equals(bicicletaId) && 
                alug.getTrancaFim() == null
            ).findFirst();
    }

    public void newAluguel(Aluguel al) {
        al.setId(UUID.randomUUID());
        this.alugueis.put(al.getId(), al);
    }

    public void updateAluguel(Aluguel al) {
        this.alugueis.put(al.getId(), al);
    }
}
