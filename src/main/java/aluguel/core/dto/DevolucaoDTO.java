package aluguel.core.dto;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DevolucaoDTO {
    private UUID tranca;
    private UUID bicicleta;
}
