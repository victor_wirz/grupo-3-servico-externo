package aluguel.network.services;

import java.util.UUID;

import aluguel.core.entities.CartaoDeCredito;
import aluguel.network.dto.CobrancaDTO;

public interface IExternoService {
    public boolean validaCartaoDeCredito(CartaoDeCredito cdc);
    
    public void sendEmail(String email, String mensagem);

    public CobrancaDTO chargeCiclista(UUID ciclista, Double valor);

    public CobrancaDTO chargeCiclistaWithQueue(UUID ciclista, Double valor);
}
