package aluguel.network.services;

import java.util.UUID;

import aluguel.network.dto.BicicletaDTO;
import aluguel.network.dto.TrancaDTO;

public interface IEquipamentoService {

    public void setBicicletaStatus(UUID bicicleta, String status);

    public TrancaDTO getTranca(UUID tranca);

    public BicicletaDTO getBicicleta(UUID bicicleta);
    
}
