package aluguel.network.services.impl;

import java.util.UUID;

import aluguel.network.dto.BicicletaDTO;
import aluguel.network.dto.TrancaDTO;
import aluguel.network.services.IEquipamentoService;

public class EquipamentoService implements IEquipamentoService {
    public void setBicicletaStatus(UUID bicicleta, String status) {
        // To be implemented
    }

    public TrancaDTO getTranca(UUID tranca) {
        TrancaDTO dto = new TrancaDTO();
        dto.setId(tranca);
        dto.setBicicleta(UUID.randomUUID());
        return dto;
    }

    public BicicletaDTO getBicicleta(UUID bicicleta) {
        BicicletaDTO dto = new BicicletaDTO();
        dto.setId(bicicleta);
        return dto;
    }
}
