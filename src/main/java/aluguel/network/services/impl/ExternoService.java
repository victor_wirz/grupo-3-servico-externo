package aluguel.network.services.impl;

import java.util.Date;
import java.util.UUID;
import aluguel.core.entities.CartaoDeCredito;
import aluguel.network.dto.CobrancaDTO;
import aluguel.network.services.IExternoService;

public class ExternoService implements IExternoService {

    public boolean validaCartaoDeCredito(CartaoDeCredito cdc) {
        return cdc.getNumero() != null;
    }
    
    public void sendEmail(String email, String mensagem) {
        // To be implemented
    }

    public CobrancaDTO chargeCiclista(UUID ciclista, Double valor) {
        CobrancaDTO dto = new CobrancaDTO();
        dto.setId(UUID.randomUUID());
        dto.setCiclista(ciclista);
        return dto;
    }

    public CobrancaDTO chargeCiclistaWithQueue(UUID ciclista, Double valor) {
        CobrancaDTO dto = new CobrancaDTO();
        dto.setCiclista(ciclista);
        dto.setHoraSolicitacao(new Date());
        return dto;
    }
    
}
