package aluguel.api.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorResponse {
    private String id;
    private String codigo;
    private String mensagem;
}
