package aluguel.api;

import aluguel.api.handlers.AluguelController;
import aluguel.api.handlers.CartaoDeCreditoController;
import aluguel.api.handlers.CiclistaController;
import aluguel.api.handlers.Controller;
import aluguel.api.handlers.FuncionarioController;
import aluguel.core.repositories.AluguelRepository;
import aluguel.core.repositories.CartaoDeCreditoRepository;
import aluguel.core.repositories.CiclistaRepository;
import aluguel.core.repositories.FuncionarioRepository;
import aluguel.network.services.impl.EquipamentoService;
import aluguel.network.services.impl.ExternoService;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {

    private Javalin app = 
        Javalin.create(config -> config.defaultContentType = "application/json")
            .routes(() -> {
                // Services
                ExternoService externoService = new ExternoService();
                EquipamentoService equipamentoService = new EquipamentoService();

                // Repositories
                CiclistaRepository ciclistaRepository = new CiclistaRepository();
                FuncionarioRepository funcionarioRepository = new FuncionarioRepository();
                CartaoDeCreditoRepository cartaoDeCreditoRepository = new CartaoDeCreditoRepository();
                AluguelRepository aluguelRepository = new AluguelRepository();
                
                // Handlers
                CiclistaController ciclistaHandler = new CiclistaController(ciclistaRepository);
                FuncionarioController funcionarioHandler = new FuncionarioController(funcionarioRepository);
                CartaoDeCreditoController cartaoHandler = new CartaoDeCreditoController(cartaoDeCreditoRepository, ciclistaRepository, externoService);
                AluguelController aluguelHandler = new AluguelController(aluguelRepository, ciclistaRepository, externoService, equipamentoService);

                get("/healthcheck", Controller::healthCheck);

                path("/ciclista", () -> {
                    get(ciclistaHandler::getAll);
                    post(ciclistaHandler::create);
                    get("/existeEmail/{email}", ctx -> ciclistaHandler.findByEmail(ctx, ctx.pathParam("email")));
                    path("/{ciclistaId}", () -> {
                        get(ciclistaHandler::getOne);
                        put(ciclistaHandler::update);
                        delete(ciclistaHandler::delete);
                        post("/ativar", ctx -> ciclistaHandler.activate(ctx, ctx.pathParam("ciclistaId")));
                    });
                });

                path("/funcionario", () -> {
                    get(funcionarioHandler::getAll);
                    post(funcionarioHandler::create);
                    path("/{funcionarioId}", () -> {
                        get(funcionarioHandler::getOne);
                        put(funcionarioHandler::update);
                        delete(funcionarioHandler::delete);
                    });
                });

                path("/cartaoDeCredito/{ciclistaId}", () -> {
                    get(cartaoHandler::getOneByCiclistaId);
                    put(cartaoHandler::update);
                });

                post("/aluguel", aluguelHandler::newAluguel);
                post("/devolucao", aluguelHandler::devolucao);

            });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
