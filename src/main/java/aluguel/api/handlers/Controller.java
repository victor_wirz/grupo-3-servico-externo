package aluguel.api.handlers;

import io.javalin.http.Context;

public class Controller {

    private Controller(){}

    public static void healthCheck(Context ctx) {
        ctx.status(200);
        ctx.result("Status OK");
    }
}
