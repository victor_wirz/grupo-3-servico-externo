package aluguel.core.entities;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AluguelTest {

    @Test
    void newAluguel() {

        UUID bicicleta = UUID.randomUUID();
        Date horaInicio = new Date();
        UUID trancaFim = UUID.randomUUID();
        Date horaFim = new Date();
        UUID cobranca = UUID.randomUUID();
        UUID ciclista = UUID.randomUUID();
        UUID trancaInicio = UUID.randomUUID();

        final Aluguel aluguel = new Aluguel(UUID.randomUUID(), bicicleta, horaInicio, trancaFim, horaFim, cobranca, ciclista, trancaInicio);
        
        assertEquals(aluguel.getBicicleta(), bicicleta);
        assertEquals(aluguel.getHoraInicio(), horaInicio);
        assertEquals(aluguel.getTrancaFim(), trancaFim);
        assertEquals(aluguel.getHoraFim(), horaFim);
        assertEquals(aluguel.getCobranca(), cobranca);
        assertEquals(aluguel.getCiclista(), ciclista);
        assertEquals(aluguel.getTrancaInicio(), trancaInicio);
    }

}
