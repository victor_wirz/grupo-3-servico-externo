package aluguel.core.entities;

import aluguel.core.enums.Nacionalidade;
import aluguel.core.enums.Status;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


class CiclistaTest {

    @Test
    void newCiclista() {
        Status status = Status.ATIVO;
        String nome = "2";
        Date nascimento = new Date();
        String cpf = "3";
        Nacionalidade nacionalidade = Nacionalidade.BRASILEIRO;
        String email = "4";

        final Ciclista ciclista = new Ciclista(UUID.randomUUID(), status, nome, nascimento, cpf, null, nacionalidade, email);
        
        assertNotNull(ciclista.getId());
        assertEquals(ciclista.getStatus(), status);
        assertEquals(ciclista.getNome(), nome);
        assertEquals(ciclista.getNascimento(), nascimento);
        assertEquals(ciclista.getCpf(), cpf);
        assertEquals(ciclista.getNacionalidade(), nacionalidade);
        assertEquals(ciclista.getEmail(), email);
        assertNull(ciclista.getPassaporte());
    }

}
