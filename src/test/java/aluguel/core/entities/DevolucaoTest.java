package aluguel.core.entities;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DevolucaoTest {

    @Test
    void newDevolucao() {

        UUID bicicleta = UUID.randomUUID();
        Date horaInicio = new Date();
        UUID trancaFim = UUID.randomUUID();
        Date horaFim = new Date();
        UUID cobranca = UUID.randomUUID();
        UUID ciclista = UUID.randomUUID();

        Devolucao devolucao = new Devolucao(bicicleta, horaInicio, trancaFim, horaFim, cobranca, ciclista);
        
        assertEquals(devolucao.getBicicleta(), bicicleta);
        assertEquals(devolucao.getHoraInicio(), horaInicio);
        assertEquals(devolucao.getTrancaFim(), trancaFim);
        assertEquals(devolucao.getHoraFim(), horaFim);
        assertEquals(devolucao.getCobranca(), cobranca);
        assertEquals(devolucao.getCiclista(), ciclista);
    }

}
