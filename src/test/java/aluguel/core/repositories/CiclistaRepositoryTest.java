package aluguel.core.repositories;

import aluguel.core.entities.Ciclista;
import aluguel.core.enums.Nacionalidade;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
class CiclistaRepositoryTest {

    @Test
    void completeCrudTest(){
        CiclistaRepository cr = new CiclistaRepository();
        Ciclista ciclista = new Ciclista();
        ciclista.setId(UUID.randomUUID());
        ciclista.setNome("test");
        ciclista.setEmail("test@email");

        // SAVE
        cr.save(ciclista);

        // GET BY ID | GET BY EMAIL | GET ALL
        Optional<Ciclista> cOne = cr.findById(ciclista.getId());
        Optional<Ciclista> cEmail = cr.findByEmail(ciclista.getEmail());
        Collection<Ciclista> ciclistas = cr.findAll();

        assertTrue(cOne.isPresent());
        assertTrue(cEmail.isPresent());
        assertFalse(ciclistas.isEmpty());

        // UPDATE
        ciclista.setNacionalidade(Nacionalidade.ESTRANGEIRO);
        cr.update(ciclista.getId(), ciclista);

        Optional<Ciclista> cOneUpdated = cr.findById(ciclista.getId());

        assertTrue(cOneUpdated.isPresent());
        assertEquals(Nacionalidade.ESTRANGEIRO, cOneUpdated.get().getNacionalidade());

        // DELETE
        cr.delete(ciclista.getId());
        Optional<Ciclista> cOneDeleted = cr.findById(ciclista.getId());
        assertFalse(cOneDeleted.isPresent());
    }
}
