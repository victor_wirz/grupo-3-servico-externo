package aluguel.core.repositories;

import aluguel.core.entities.Funcionario;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class FuncionarioRepositoryTest {

    @Test
    void completeCrudTest(){
        FuncionarioRepository cr = new FuncionarioRepository();
        Funcionario funcionario = new Funcionario();
        funcionario.setMatricula(UUID.randomUUID().toString());
        funcionario.setNome("test");
        funcionario.setEmail("test@email");

        // SAVE
        cr.save(funcionario);

        // GET BY ID | GET ALL
        Optional<Funcionario> cOne = cr.findById(funcionario.getMatricula());
        Collection<Funcionario> funcionarios = cr.findAll();

        assertTrue(cOne.isPresent());
        assertFalse(funcionarios.isEmpty());

        // UPDATE
        funcionario.setFuncao("testFunc");
        cr.update(funcionario.getMatricula(), funcionario);

        Optional<Funcionario> cOneUpdated = cr.findById(funcionario.getMatricula());

        assertTrue(cOneUpdated.isPresent());
        assertEquals("testFunc", cOneUpdated.get().getFuncao());

        // DELETE
        cr.delete(funcionario.getMatricula());
        Optional<Funcionario> cOneDeleted = cr.findById(funcionario.getMatricula());
        assertFalse(cOneDeleted.isPresent());
    }
}
