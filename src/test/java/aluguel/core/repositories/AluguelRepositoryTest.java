package aluguel.core.repositories;

import aluguel.core.entities.Aluguel;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
class AluguelRepositoryTest {

    @Test
    void completeCrudTest(){
        AluguelRepository ar = new AluguelRepository();
        Aluguel al = new Aluguel();

        UUID ciclistaId = UUID.randomUUID();
        UUID bicicletaId = UUID.randomUUID();
        al.setBicicleta(bicicletaId);
        al.setCiclista(ciclistaId);
        al.setCobranca(UUID.randomUUID());
        al.setHoraInicio(new Date());
        al.setTrancaInicio(UUID.randomUUID());

        ar.newAluguel(al);

        Optional<Aluguel> ongoingAluguel = ar.getOngoingAluguelByCiclista(ciclistaId);
        assertTrue(ongoingAluguel.isPresent());
        assertEquals(ongoingAluguel.get().getBicicleta(), al.getBicicleta());
        assertEquals(ongoingAluguel.get().getCobranca(), al.getCobranca());
        assertEquals(ongoingAluguel.get().getTrancaInicio(), al.getTrancaInicio());

        Optional<Aluguel> ongoingAluguelByBicicleta = ar.getOngoingAluguelByBicicleta(bicicletaId);
        assertTrue(ongoingAluguelByBicicleta.isPresent());
        assertEquals(ongoingAluguelByBicicleta.get().getBicicleta(), al.getBicicleta());
        assertEquals(ongoingAluguelByBicicleta.get().getCobranca(), al.getCobranca());
        assertEquals(ongoingAluguelByBicicleta.get().getTrancaInicio(), al.getTrancaInicio());

        al.setHoraFim(new Date());
        al.setTrancaFim(UUID.randomUUID());

        ar.updateAluguel(al);

        Optional<Aluguel> ongoingAluguelAfterUpdate = ar.getOngoingAluguelByCiclista(ciclistaId);
        assertFalse(ongoingAluguelAfterUpdate.isPresent());
    }
}
