package aluguel.api.handlers;

import aluguel.api.JavalinApp;
import aluguel.core.entities.CartaoDeCredito;
import aluguel.core.entities.Ciclista;
import aluguel.core.enums.Status;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class CartaoDeCreditoControllerTest {

    private static JavalinApp app;
    private static String baseUrl;

    @BeforeAll
    static void init() {
        int port = 7013;
        baseUrl = "http://localhost:" + port;
        app = new JavalinApp();
        app.start(port);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void getTest() {
        final HttpResponse response = Unirest.get(baseUrl + "/cartaoDeCredito/0ef4982c-9569-4226-93bf-945fb4d8897b").asString();
        assertEquals(200, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void putCartaoDeCredito_success() {
        CartaoDeCredito cdc = new CartaoDeCredito();
        cdc.setNomeTitular("titularTest");
        cdc.setNumero("1");
        cdc.setCvv("987");

        final HttpResponse response = Unirest
                .put(baseUrl + "/cartaoDeCredito/0ef4982c-9569-4226-93bf-945fb4d8897b")
                .body(cdc)
                .asString();
        assertEquals(204, response.getStatus());
        assertEquals("", response.getBody());
    }

    @Test
    void putCartaoDeCredito_fail_nonexistent_ciclista() {
        CartaoDeCredito cdc = new CartaoDeCredito();
        cdc.setNomeTitular("titularTest");
        cdc.setNumero("1");
        cdc.setCvv("987");

        final HttpResponse response = Unirest
                .put(baseUrl + "/cartaoDeCredito/0ef4982c-9569-4226-93bf-999999999999")
                .body(cdc)
                .asString();
        assertEquals(404, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void putCartao_fail_invalid_format() {
        Ciclista f = new Ciclista();
        f.setNome("ciclista");
        f.setStatus(Status.AGUARDANDO_CONFIRMACAO);

        final HttpResponse response = Unirest
                .put(baseUrl + "/cartaoDeCredito/0ef4982c-9569-4226-93bf-945fb4d8897b")
                .body(f)
                .asString();
        assertEquals(422, response.getStatus());
        assertNotNull(response.getBody());
    }

}