package aluguel.api.handlers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import aluguel.core.dto.AluguelDTO;
import aluguel.core.dto.DevolucaoDTO;
import aluguel.core.entities.Aluguel;
import aluguel.core.entities.Ciclista;
import aluguel.core.repositories.AluguelRepository;
import aluguel.core.repositories.CiclistaRepository;
import aluguel.network.dto.BicicletaDTO;
import aluguel.network.dto.CobrancaDTO;
import aluguel.network.dto.TrancaDTO;
import aluguel.network.services.IEquipamentoService;
import aluguel.network.services.IExternoService;
import io.javalin.http.Context;

class AluguelControllerTest {

    AluguelRepository aluguelRepositoryMock = mock(AluguelRepository.class);
    CiclistaRepository ciclistaRepositoryMock = mock(CiclistaRepository.class);
    IExternoService externoServiceMock = mock(IExternoService.class);
    IEquipamentoService equipamentoServiceMock = mock(IEquipamentoService.class);
    Context ctx = mock(Context.class);
    
    @Test
    void newAluguel_Success(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        AluguelDTO dto = new AluguelDTO();
        dto.setCiclista(UUID.randomUUID());
        dto.setTrancaInicio(UUID.randomUUID());

        Ciclista c = new Ciclista();
        c.setId(dto.getCiclista());

        TrancaDTO t = new TrancaDTO();
        t.setId(UUID.randomUUID());
        t.setBicicleta(UUID.randomUUID());

        BicicletaDTO b = new BicicletaDTO();
        b.setId(t.getBicicleta());

        CobrancaDTO cob = new CobrancaDTO();
        cob.setId(UUID.randomUUID());

        doReturn(dto).when(ctx).bodyAsClass(AluguelDTO.class);
        when(ciclistaRepositoryMock.findById(dto.getCiclista())).thenReturn(Optional.of(c));
        when(equipamentoServiceMock.getTranca(dto.getTrancaInicio())).thenReturn(t);
        when(equipamentoServiceMock.getBicicleta(t.getBicicleta())).thenReturn(b);
        when(externoServiceMock.chargeCiclista(dto.getCiclista(), 10.0)).thenReturn(cob);

        aluguelRepositoryMock.newAluguel(any(Aluguel.class));

        // Act
        aluguelController.newAluguel(ctx);

        // Assert
        verify(ctx).status(200);
        verify(ctx).json(isNotNull());
    }

    @Test
    void newAluguel_fail_cobranca(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        AluguelDTO dto = new AluguelDTO();
        dto.setCiclista(UUID.randomUUID());
        dto.setTrancaInicio(UUID.randomUUID());

        Ciclista c = new Ciclista();
        c.setId(dto.getCiclista());

        TrancaDTO t = new TrancaDTO();
        t.setId(UUID.randomUUID());
        t.setBicicleta(UUID.randomUUID());

        BicicletaDTO b = new BicicletaDTO();
        b.setId(t.getBicicleta());

        doReturn(dto).when(ctx).bodyAsClass(AluguelDTO.class);
        when(ciclistaRepositoryMock.findById(dto.getCiclista())).thenReturn(Optional.of(c));
        when(equipamentoServiceMock.getTranca(dto.getTrancaInicio())).thenReturn(t);
        when(equipamentoServiceMock.getBicicleta(t.getBicicleta())).thenReturn(b);
        when(externoServiceMock.chargeCiclista(dto.getCiclista(), 10.0)).thenReturn(null);

        // Act
        aluguelController.newAluguel(ctx);

        // Assert
        verify(ctx).status(422);
        verify(ctx).json(isNotNull());
    }

    @Test
    void newAluguel_fail_bicicleta(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        AluguelDTO dto = new AluguelDTO();
        dto.setCiclista(UUID.randomUUID());
        dto.setTrancaInicio(UUID.randomUUID());

        Ciclista c = new Ciclista();
        c.setId(dto.getCiclista());

        TrancaDTO t = new TrancaDTO();
        t.setId(UUID.randomUUID());
        t.setBicicleta(UUID.randomUUID());

        doReturn(dto).when(ctx).bodyAsClass(AluguelDTO.class);
        when(ciclistaRepositoryMock.findById(dto.getCiclista())).thenReturn(Optional.of(c));
        when(equipamentoServiceMock.getTranca(dto.getTrancaInicio())).thenReturn(t);
        when(equipamentoServiceMock.getBicicleta(t.getBicicleta())).thenReturn(null);

        // Act
        aluguelController.newAluguel(ctx);

        // Assert
        verify(ctx).status(422);
        verify(ctx).json(isNotNull());
    }

    @Test
    void newAluguel_fail_tranca(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        AluguelDTO dto = new AluguelDTO();
        dto.setCiclista(UUID.randomUUID());
        dto.setTrancaInicio(UUID.randomUUID());

        Ciclista c = new Ciclista();
        c.setId(dto.getCiclista());

        doReturn(dto).when(ctx).bodyAsClass(AluguelDTO.class);
        when(ciclistaRepositoryMock.findById(dto.getCiclista())).thenReturn(Optional.of(c));
        when(equipamentoServiceMock.getTranca(dto.getTrancaInicio())).thenReturn(null);

        // Act
        aluguelController.newAluguel(ctx);

        // Assert
        verify(ctx).status(422);
        verify(ctx).json(isNotNull());
    }

    @Test
    void newAluguel_fail_ciclista(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        AluguelDTO dto = new AluguelDTO();
        dto.setCiclista(UUID.randomUUID());
        dto.setTrancaInicio(UUID.randomUUID());

        doReturn(dto).when(ctx).bodyAsClass(AluguelDTO.class);
        when(ciclistaRepositoryMock.findById(dto.getCiclista())).thenReturn(Optional.empty());

        // Act
        aluguelController.newAluguel(ctx);

        // Assert
        verify(ctx).status(422);
        verify(ctx).json(isNotNull());
    }

    @Test
    void newAluguel_fail_body_format(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        Ciclista dto = new Ciclista();
        dto.setId(UUID.randomUUID());

        doReturn(dto).when(ctx).bodyAsClass(AluguelDTO.class);

        // Act
        aluguelController.newAluguel(ctx);

        // Assert
        verify(ctx).status(400);
        verify(ctx).json(isNotNull());
    }

    @Test
    void devolucao_Success(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        DevolucaoDTO dto = new DevolucaoDTO();
        dto.setBicicleta(UUID.randomUUID());
        dto.setTranca(UUID.randomUUID());

        Date dateNow = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateNow);
        cal.add(Calendar.HOUR, -3);

        Date hoursAgo = cal.getTime();

        Aluguel aluguel = new Aluguel();
        aluguel.setBicicleta(dto.getBicicleta());
        aluguel.setTrancaInicio(UUID.randomUUID());
        aluguel.setCiclista(UUID.randomUUID());
        aluguel.setHoraInicio(hoursAgo);

        CobrancaDTO cob = new CobrancaDTO();
        cob.setId(UUID.randomUUID());

        Ciclista c = new Ciclista();
        c.setId(aluguel.getCiclista());
        c.setEmail("test@test.com");

        doReturn(dto).when(ctx).bodyAsClass(DevolucaoDTO.class);
        when(aluguelRepositoryMock.getOngoingAluguelByBicicleta(dto.getBicicleta())).thenReturn(Optional.of(aluguel));
        when(externoServiceMock.chargeCiclistaWithQueue(aluguel.getCiclista(), 20.0)).thenReturn(cob);
        when(ciclistaRepositoryMock.findById(c.getId())).thenReturn(Optional.of(c));
        externoServiceMock.sendEmail(anyString(), anyString());
        aluguelRepositoryMock.updateAluguel(any(Aluguel.class));

        // Act
        aluguelController.devolucao(ctx);

        // Assert
        verify(ctx).status(200);
        verify(ctx).json(isNotNull());
    }

    @Test
    void devolucao_fail_get_ongoing(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        DevolucaoDTO dto = new DevolucaoDTO();
        dto.setBicicleta(UUID.randomUUID());
        dto.setTranca(UUID.randomUUID());

        doReturn(dto).when(ctx).bodyAsClass(DevolucaoDTO.class);
        when(aluguelRepositoryMock.getOngoingAluguelByBicicleta(dto.getBicicleta())).thenReturn(Optional.empty());

        // Act
        aluguelController.devolucao(ctx);

        // Assert
        verify(ctx).status(404);
        verify(ctx).json(isNotNull());
    }

    @Test
    void devolucao_fail_body_format(){

        // Arrange
        AluguelController aluguelController = new AluguelController(aluguelRepositoryMock, ciclistaRepositoryMock, externoServiceMock, equipamentoServiceMock);

        Ciclista dto = new Ciclista();
        dto.setId(UUID.randomUUID());

        doReturn(dto).when(ctx).bodyAsClass(DevolucaoDTO.class);

        // Act
        aluguelController.devolucao(ctx);

        // Assert
        verify(ctx).status(400);
        verify(ctx).json(isNotNull());
    }

}
