package aluguel.api.handlers;

import aluguel.api.JavalinApp;
import aluguel.core.entities.Ciclista;
import aluguel.core.entities.Funcionario;
import aluguel.core.enums.Status;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class FuncionarioControllerTest {

    private static JavalinApp app;
    private static String baseUrl;

    @BeforeAll
    static void init() {
        int port = 7012;
        baseUrl = "http://localhost:" + port;
        app = new JavalinApp();
        app.start(port);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void getTests() {
        class getTest {
            public final String path;
            public final int returnCode;

            getTest(String path, int code) {
                this.path = path;
                this.returnCode = code;
            }
        }
        getTest t1 = new getTest("/funcionario", 200);
        getTest t2 = new getTest("/funcionario/0ef4982c9569422693bf945fb4d8897b", 200);
        getTest t3 = new getTest("/funcionario/0ef4982c9569422693bf949999999999", 404);

        List<getTest> testList = Arrays.asList(t1, t2, t3);
        for (getTest test : testList) {
            final HttpResponse response = Unirest.get(baseUrl + test.path).asString();
            assertEquals(test.returnCode, response.getStatus());
            assertNotNull(response.getBody());
        }

    }

    @Test
    void postFuncionario_success() {
        Funcionario f = new Funcionario();
        f.setNome("test");
        f.setEmail("email@email");
        f.setCpf("xxx.xxx.xxx.xx");

        final HttpResponse response = Unirest.post(baseUrl + "/funcionario").body(f).asString();
        assertEquals(201, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void postFuncionario_fail() {
        String body = "'{\n" +
                "        \"nome\": \"funcionario\",\n" +
                "        \"nascimento\": \"2022232-07-03\",\n" +
                "        \"cpf\": \"xxx.xxx.xxx-xx\",\n" +
                "        \"passaporte\": {\n" +
                "        \"numero\": \"xx\",\n" +
                "        \"validade\": \"2030-07-03\",\n" +
                "        \"pais\": \"BR\"\n" +
                "        },\n" +
                "        \"nacionalidade\": \"sadasd\",\n" +
                "        \"email\": \"funcionario@email.com\"\n" +
                "        }'";

        final HttpResponse response = Unirest.post(baseUrl + "/funcionario").body(body).asString();
        assertEquals(422, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void putFuncionario_success() {
        Funcionario f = new Funcionario();
        f.setNome("funcionarioUpdated");
        f.setFuncao("func");
        f.setEmail("test@email");
        f.setCpf("xxx.xxx.xxx-xx");
        f.setIdade(20);
        f.setSenha("testSenha");

        final HttpResponse response = Unirest
                .put(baseUrl + "/funcionario/0ef4982c9569422693bf945fb4d8897b")
                .body(f)
                .asString();
        assertEquals(204, response.getStatus());
        assertEquals("", response.getBody());
    }

    @Test
    void putFuncionario_fail_nonexistent() {
        Funcionario f = new Funcionario();
        f.setNome("funcionarioUpdated");
        f.setFuncao("func");
        f.setEmail("test@email");
        f.setCpf("xxx.xxx.xxx-xx");
        f.setIdade(20);
        f.setSenha("testSenha");

        final HttpResponse response = Unirest
                .put(baseUrl + "/funcionario/0ef4982c9569422693bf999999999999")
                .body(f)
                .asString();
        assertEquals(404, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void putFuncionario_fail_422() {
        Ciclista f = new Ciclista();
        f.setNome("ciclista");
        f.setStatus(Status.AGUARDANDO_CONFIRMACAO);

        final HttpResponse response = Unirest
                .put(baseUrl + "/funcionario/0ef4982c-9569-4226-93bf-945fb4d8897b")
                .body(f)
                .asString();
        assertEquals(422, response.getStatus());
        assertNotNull(response.getBody());
    }

    @Test
    void delete_success() {
        final HttpResponse response = Unirest
                .delete(baseUrl + "/funcionario/fcf9c76aa99f42e7ba0d590857b87594")
                .asString();
        assertEquals(204, response.getStatus());
        assertEquals("", response.getBody());
    }

}