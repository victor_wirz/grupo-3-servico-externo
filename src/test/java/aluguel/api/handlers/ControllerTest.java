package aluguel.api.handlers;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import aluguel.api.JavalinApp;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ControllerTest {

    private static JavalinApp app;

    @BeforeAll
    static void init() {
        app = new JavalinApp();
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void healthCheckTest() {
        final HttpResponse response = Unirest.get("http://localhost:7010/healthcheck").asString();
        assertEquals(200, response.getStatus());
        assertEquals("Status OK", response.getBody());
    }
}
